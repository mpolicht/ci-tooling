// Provisionally mark the build as successful
currentBuild.result = 'SUCCESS'

// Request a node to be allocated to us
node( currentPlatform ) {
// We want Timestamps on everything
timestamps {
	// We want to catch any errors that occur to allow us to send out notifications (ie. emails) if needed
	catchError {
		// First Thing: Checkout Sources
		stage('Checkout Sources') {
			// Make sure we have a clean slate to begin with
			deleteDir()

			// This includes any potential install directory
			bat """
				if exist "C:\\CI\\Software Installs\\${projectName}" rmdir /s /q "C:\\CI\\Software Installs\\${projectName}"
			"""
		
			// Actual Application Sources
			checkout changelog: true, poll: true, scm: [
				$class: 'GitSCM',
				branches: [[name: branchToBuild]],
				browser: [$class: 'CGit', repoUrl: browserUrl],
				extensions: [[$class: 'CloneOption', timeout: 120]],
				userRemoteConfigs: [[url: repositoryUrl]]
			]

			// Our CI scripts
			checkout changelog: false, poll: false, scm: [
				$class: 'GitSCM',
				branches: [[name: 'master']],
				extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: 'ci-tooling/']],
				userRemoteConfigs: [[url: 'https://anongit.kde.org/sysadmin/ci-tooling']]
			]

			// Projects metadata and next generation dependency metadata
			checkout changelog: false, poll: false, scm: [
				$class: 'GitSCM',
				branches: [[name: 'master']],
				extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: 'ci-tooling/repo-metadata/']],
				userRemoteConfigs: [[url: 'https://anongit.kde.org/sysadmin/repo-metadata']]
			]

			// Dependency Metadata
			checkout changelog: false, poll: false, scm: [
				$class: 'GitSCM',
				branches: [[name: 'master']],
				extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: 'ci-tooling/kde-build-metadata/']],
				userRemoteConfigs: [[url: 'https://anongit.kde.org/kde-build-metadata']]
			]

		}

		// Now Prepare to Build: Get the dependencies ready
		stage('Setup Dependencies') {
			// Now we can determine what our dependencies are
			// Then update to the latest version of the dependencies available from the master server
			// Finally extract all of those dependencies in turn into the given 'installTo' directory
			bat """
				python -u ci-tooling/helpers/prepare-dependencies.py --product ${productName} --project ${projectName} --branchGroup ${branchGroup} --environment ${ciEnvironment} --platform ${currentPlatform} --installTo "C:/CI/Software Installs/${projectName}/"
			"""
		}

		// Now we can configure our build
		stage('Configuring Build') {
			// This is delegated through a helper script to handle minor special cases like inSourceBuilds, non-CMake build systems, etc
			bat """
				call "C:/Program Files (x86)/Microsoft Visual Studio/2019/Professional/VC/Auxiliary/Build/vcvars64.bat" -vcvars_ver=14.16
				set CRAFT_ROOT=C:/Craft/CI-Qt511/windows-msvc2017_64-cl-debug/
				python -u ci-tooling/helpers/configure-build.py --product ${productName} --project ${projectName} --branchGroup ${branchGroup} --platform ${currentPlatform} --installTo "C:/CI/Software Installs/${projectName}/"
			"""
		}

		// Finally we can build it! (Once again, through a helper)
		stage('Compiling') {
			// We use a helper here so we can determine the appropriate number of CPUs (-j) to build with
			bat """
				call "C:/Program Files (x86)/Microsoft Visual Studio/2019/Professional/VC/Auxiliary/Build/vcvars64.bat" -vcvars_ver=14.16
				set CRAFT_ROOT=C:/Craft/CI-Qt511/windows-msvc2017_64-cl-debug/
				python -u ci-tooling/helpers/compile-build.py --product ${productName} --project ${projectName} --branchGroup ${branchGroup} --platform ${currentPlatform} --usingInstall "C:/CI/Software Installs/${projectName}/"
			"""
		}

		// Now ensure that it installs....
		stage('Installing') {
			// The helper ensures that DESTDIR and INSTALL_ROOT are set to 'divertTo'
			// This allows us to capture the install at the next stage for later reuse in the Setup Dependencies step
			bat """
				call "C:/Program Files (x86)/Microsoft Visual Studio/2019/Professional/VC/Auxiliary/Build/vcvars64.bat" -vcvars_ver=14.16
				set CRAFT_ROOT=C:/Craft/CI-Qt511/windows-msvc2017_64-cl-debug/
				python -u ci-tooling/helpers/install-build.py --product ${productName} --project ${projectName} --branchGroup ${branchGroup} --platform ${currentPlatform} --installTo "C:/CI/Software Installs/${projectName}/" --divertTo "%WORKSPACE%/install-divert/"
			"""
		}

		// Looks like it built okay - let's capture this for later use
		// We'll also take the opportunity to extract metadata from CMake used by packagers and api.kde.org
		stage('Capturing Installation') {
			// First we create a tar archive of the installation which was diverted
			// Then we upload a copy of that to the master server and have it publish the new archive
			// Finally to save bandwidth our copy of the tar archive is moved to our local cache for reuse on later builds on this node
			bat """
				python -u ci-tooling/helpers/capture-install.py --product ${productName} --project ${projectName} --branchGroup ${branchGroup} --environment ${ciEnvironment} --platform ${currentPlatform} --divertedTo "%WORKSPACE%/install-divert/" --installedTo "C:/CI/Software Installs/${projectName}/"
			"""
		}

		// Now we can run our tests
		stage('Running Tests') {
			// Run the unit tests for this project
			// Tests are run in a basic environment (X, DBus)
			bat """
				call "C:/Program Files (x86)/Microsoft Visual Studio/2019/Professional/VC/Auxiliary/Build/vcvars64.bat" -vcvars_ver=14.16
				set CRAFT_ROOT=C:/Craft/CI-Qt511/windows-msvc2017_64-cl-debug/
				python -u ci-tooling/helpers/run-tests.py --product ${productName} --project ${projectName} --branchGroup ${branchGroup} --platform ${currentPlatform} --usingInstall "C:/CI/Software Installs/${projectName}/"
			"""

			// Collect our results
			junit allowEmptyResults: true, testResults: 'JUnitTestResults.xml'
		}

		// Final thing to do: some code quality checks
		stage('Checking Code Quality') {

			// cppcheck is not supported by Pipeline at the moment, so we don't run that for now
			// See https://issues.jenkins-ci.org/browse/JENKINS-35096

			// Cobertura doesn't support Pipeline either, so no code coverage publishing...
			// See https://issues.jenkins-ci.org/browse/JENKINS-30700

			// Scan the logs and publish a warnings report
			warnings consoleParsers: [[parserName: 'MSBuild']], excludePattern: '(C|c):/(Program Files \\(x86\\)|program files \\(x86\\)|Craft|craft)/.*'

		}

	}

	// Let's determine if we need to send out notifications
	// What happened in our previous build?
	def previousResult = currentBuild.previousBuild?.result
	// If our condition has changed or is FAILURE then we want to send an email
	if( previousResult != currentBuild.result || currentBuild.result == 'FAILURE' ) {
		// Start constructing the list of our recipients
		// At this point we can only be either a failure or an unstable build, so notify those who have requested unstable build notifications
		def mailTo = [ unstableBuildEmails ]

		// If the build was a solid failure (either now or previously) then notify those who want to know about failures only
		if( previousResult == 'FAILURE' || currentBuild.result == 'FAILURE' ) {
			// Add them to the list
			mailTo << buildFailureEmails
		}

		// If someone kicked this job off, they're presumably interested as well
		mailTo << emailextrecipients( [[$class: 'RequesterRecipientProvider']] )
		// We always want to notify our dashboard as well
		mailTo << "kde-dashboard@kde.org"

		// Finalise the list of recipients
		mailTo = mailTo.join(',')

		// Send the email now
		emailext(
			to: mailTo,
			body: '${JELLY_SCRIPT,template="html_gmail"}',
			mimeType: 'text/html',
			subject: 'KDE CI: ${PROJECT_NAME} - Build # ${BUILD_NUMBER} - ${BUILD_STATUS}!', 
			attachLog: false
		)
	}

	// IRC Notifications are currently not supported by Pipeline
	// See https://issues.jenkins-ci.org/browse/JENKINS-33922
	// We can probably workaround this using Pursuivant and the emails Jenkins sends out
	// This would allow subscribing to build notifications for IRC channels in much the same way one subscribes for Commits and Bugzilla changes

	// As the Windows Slaves are permanent ones, we erase the Workspace as the last thing we do
	deleteDir()

	// Cleanup the installation directory too
	bat """
		if exist "C:\\CI\\Software Installs\\${projectName}" rmdir /s /q "C:\\CI\\Software Installs\\${projectName}"
	"""

}
}
