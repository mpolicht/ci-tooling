// Read the contents of the gathered-structure.json file a step created for us previously
def structureToParse = readFileFromWorkspace('gathered-structure.json')
def knownStructure = new groovy.json.JsonSlurper().parseText( structureToParse )

// Prepare a list of supported OSes for later use
def supportedOSes = [
	'Linux':   '.* SUSEQt5.[0-9]+$',
	'FreeBSD': '.* FreeBSDQt5.12$',
	'Windows': '.* WindowsMSVCQt5.[0-9]+$'
]

// Setup a view for each OS we support
supportedOSes.each {
	// Some preparation to create the view...
	def jobRegex = "^${it.value}"
	def viewName = "OS - ${it.key}"
	def osName   = "${it.key}"

	// Now create the view
	listView( viewName ) {
		// Give it a nice description
		description("Current Status of ${osName} builds")
		// We want to recurse when looking for our jobs
		recurse()
		// Only include jobs matching the previously provided regex
		jobs {
			regex( jobRegex )
		}
		// Include a reasonable/sane set of Columns
		columns {
			status()
			weather()
			name()
			lastSuccess()
			lastFailure()
			lastDuration()
			buildButton()
		}
	}
}

// Setup a global failing view
listView( "Failing" ) {
	// Give it a nice description
	description("Jobs currently failing on the CI system")
	// We want to recurse when looking for our jobs
	recurse()
	// Ensure we match all jobs by name, but only include failures...
	jobFilters {
		status {
			status(Status.FAILED)
		}
	}
	// Include a reasonable/sane set of Columns
	columns {
		status()
		weather()
		name()
		lastSuccess()
		lastFailure()
		lastDuration()
		buildButton()
	}
}

// Setup a recursive view inside each Platform's folder so people can see a big overall view should they so wish
knownStructure.products.each {
	// Prepare to create the view
	def product  = "${it}"
	def viewName = "${it}/Everything"

	// Now create the view
	listView( viewName ) {
		// Give it a nice description
		description("Current Status of all ${product} builds")
		// We want to recurse when looking for our jobs
		recurse()
		// Only include jobs matching the previously provided regex
		jobs {
			regex( '.* .*' )
		}
		// Include a reasonable/sane set of Columns
		columns {
			status()
			weather()
			name()
			lastSuccess()
			lastFailure()
			lastDuration()
			buildButton()
		}
	}
}

// Setup per Branch Group and per Platform views inside each Product as well for those wanting to monitor on that level
knownStructure.combinations.each {
	// Grab all the info out for later use
	def product      = "${it.product}"
	def branchGroup  = "${it.branchGroup}"
	def platform     = "${it.platform}"

	// Create the view for the overall branch group view
	listView( "${it.product}/Everything - ${it.branchGroup}" ) {
		// Give it a nice description
		description("Current Status of all ${product} builds")
		// We want to recurse when looking for our jobs
		recurse()
		// Only include jobs matching the previously provided regex
		jobs {
			regex( ".*${branchGroup}.*" )
		}
		// Include a reasonable/sane set of Columns
		columns {
			status()
			weather()
			name()
			lastSuccess()
			lastFailure()
			lastDuration()
			buildButton()
		}
	}

	// Create the view for the platform level view
	listView( "${it.product}/Platform - ${it.platform}" ) {
		// Give it a nice description
		description("Current Status of all ${product} builds on ${platform}")
		// We want to recurse when looking for our jobs
		recurse()
		// Only include jobs matching the previously provided regex
		jobs {
			regex( ".* ${platform}" )
		}
		// Include a reasonable/sane set of Columns
		columns {
			status()
			weather()
			name()
			lastSuccess()
			lastFailure()
			lastDuration()
			buildButton()
		}
	}
}
