#!/usr/bin/python3
import os
import sys
import argparse
import subprocess
from helperslib import BuildSpecs, BuildSystem, CommonUtils, EnvironmentHandler, Packages

# Parse the command line arguments we've been given
parser = argparse.ArgumentParser(description='Utility to extract CMake Dependency Metadata from a build system. Provided to assist packagers with their packages.')
parser.add_argument('--product', type=str, required=True)
parser.add_argument('--project', type=str, required=True)
parser.add_argument('--branchGroup', type=str, required=True)
parser.add_argument('--platform', type=str, required=True)
parser.add_argument('--usingInstall', type=str, required=True)
arguments = parser.parse_args()

# Load our build specification, which governs how we handle this build
buildSpecification = BuildSpecs.Loader( product=arguments.product, project=arguments.project, branchGroup=arguments.branchGroup, platform=arguments.platform )

# Determine the environment we need to provide for the compilation process
buildEnvironment = EnvironmentHandler.generateFor( installPrefix=arguments.usingInstall )

# Determine where our source code is checked out to and where we will be building it
# We'll assume that the directory we're running from is where the sources are located
sourcesLocation = os.getcwd()
buildLocation = CommonUtils.buildDirectoryForSources( sources=sourcesLocation, inSourceBuild=buildSpecification['in-source-build'] )

# Are we allowed to run?
# We only gather this metadata from the principal Linux platform, which at the moment is SUSEQt5.10 for Frameworks and everyone else
if arguments.platform != 'SUSEQt5.10':
	# Then there is nothing for us to do
	sys.exit(0)

# Determine the name we'll use to store the results, as well as the local and remote paths it will be stored at
# We use the package name as it's pretty much guaranteed to be unique among all the builds we will be running
# As the CMake dependency metadata is only used by Linux packagers, we don't need to take platform into account here
dependencyFilename = Packages.nameForProject(arguments.product, arguments.project, arguments.branchGroup) + '.json'
localDependencyFilename = os.path.join( sourcesLocation, dependencyFilename )
remoteDependencyFilename = os.path.join( '/srv/dependency-metadata/', dependencyFilename )

# Build the command to run
commandToRun = "python3 '{0}/kde-dev-scripts/cmake-dependencies.py'"
commandToRun = commandToRun.format( CommonUtils.scriptsBaseDirectory() )

# Time to run the command - open our dependency metadata file...
with open(localDependencyFilename, 'w') as localDependencyFile:
	# Now run the command
	# We redirect stdout and stderr into the file because this program prints it's results to the console (stdout/stderr)
	process = subprocess.Popen( commandToRun, stdout=localDependencyFile, stderr=localDependencyFile, shell=True, env=buildEnvironment, cwd=buildLocation )
	process.wait()

# Now we transfer it to it's final home - establish the ssh connection
privateKeyFile = os.path.join( os.path.expanduser('~'), 'Keys', 'cmake-dependencies.key')
client = CommonUtils.establishSSHConnection( 'charlotte.kde.org', 'dependencymetadata', privateKeyFile )
# Bring up a SFTP session
sftp = client.open_sftp()

# Transfer it there
sftp.put( localDependencyFilename, remoteDependencyFilename )

# All done, cleanup
sftp.close()
client.close()
