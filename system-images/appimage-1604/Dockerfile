FROM ubuntu:16.04

LABEL Description="KDE Appimage Base"
MAINTAINER KDE Sysadmin <sysadmin@kde.org>

# Start off as root
USER root

# Make sure we are fully up to date to start with and install our core operational requirements
RUN apt-get update && apt-get install -y software-properties-common python-software-properties && add-apt-repository -y ppa:openjdk-r/ppa
RUN apt-get update && apt-get upgrade -y && apt-get install -y openssh-server openjdk-8-jre-headless

# Some software demands a newer GCC because they're using C++14 stuff, which is just insane
RUN add-apt-repository -y ppa:ubuntu-toolchain-r/test && apt-get update

# Now install the general dependencies we need for builds
RUN apt-get install -y \
  # General requirements for building KDE software
  build-essential cmake git-core locales \
  # General requirements for building other software
  automake gcc-6 g++-6 libxml-parser-perl libpq-dev libaio-dev \
  # Needed for some frameworks
  bison gettext \
  # Qt and KDE Build Dependencies
  gperf libasound2-dev libatkmm-1.6-dev libbz2-dev libcairo-perl libcap-dev libcups2-dev libdbus-1-dev \
  libdrm-dev libegl1-mesa-dev libfontconfig1-dev libfreetype6-dev libgcrypt11-dev libgl1-mesa-dev \
  libglib-perl libgsl0-dev libgstreamer-plugins-base0.10-dev libgstreamer0.10-dev \
  libgtk2-perl libjpeg-dev libnss3-dev libpci-dev libpng12-dev libpulse-dev libssl-dev \
  libtiff5-dev libudev-dev libwebp-dev flex libmysqlclient-dev \
  # Mesa libraries for everything to use
  libx11-dev libxcb-glx0-dev libxcb-keysyms1-dev libxcb-util0-dev libxcb1-dev libxcomposite-dev libxcursor-dev \
  libxdamage-dev libxext-dev libxfixes-dev libxi-dev libxrandr-dev libxrender-dev libxss-dev libxtst-dev mesa-common-dev \
  # Kdenlive AppImage extra dependencies
  liblist-moreutils-perl libtool libpixman-1-dev subversion

# Setup a user account for everything else to be done under
RUN useradd -d /home/appimage/ -u 1000 --user-group --create-home -G video appimage
# Make sure SSHD will be able to startup
RUN mkdir /var/run/sshd/
# Get locales in order
RUN locale-gen en_US en_US.UTF-8 en_NZ.UTF-8

# Switch over to our new user and add in the utilities needed for appimage builds
USER appimage
COPY setup-utilities /home/appimage/
RUN /home/appimage/setup-utilities

# Now we go back to being root for the final phase
USER root

# We want to run SSHD so that Jenkins can remotely connect to this container
EXPOSE 22
CMD ["/usr/sbin/sshd", "-D"]
