FROM opensuse/tumbleweed
MAINTAINER openSUSE KDE Maintainers <opensuse-kde@opensuse.org>

# Add KDE:Qt:5.12 repo
RUN zypper --non-interactive addrepo --priority 50 --refresh obs://KDE:Qt:5.12/openSUSE_Tumbleweed KDE:Qt:5.12
# Update container, import GPG key for KUQ
RUN zypper --non-interactive --gpg-auto-import-keys -v dup
# Install various other packages
RUN zypper --non-interactive install java-1_8_0-openjdk-headless python3-lxml python3-paramiko python3-PyYAML python3-simplejson
# Install build dependencies
RUN zypper --non-interactive install --recommends -t pattern devel_qt5 devel_C_C++
# The pattern is likely not enough, so just install all Qt devel packages from KUQ
RUN zypper -q se --not-installed-only --repo KDE:Qt:5.12 libqt5*devel libQt5*devel | tail -n +4 | cut -d "|" -f 2 | grep -v "libqt5-creator" | grep -v "libqt5-qtvirtualkeyboard-private-headers" | grep -v "libQt5HunspellInputMethod-private-headers" | grep -vi "libqt5xdg" | grep -v "libQt5Pas" | xargs zypper --non-interactive in
# And some other useful and base packages
RUN zypper --non-interactive in git clang python3-Sphinx python-qt5 python3-qt5 xvfb-run AppStream python3-pip ruby-devel libffi-devel openbox sassc \
    # temporarily: curl needed for appstreamcli, cmp. https://bugzilla.opensuse.org/show_bug.cgi?id=1080446
    curl \
    # abi tracking software and it's dependencies
    abi-compliance-checker ctags \
    # basic Qt5 packages, which have no -devel and should be manually installed
    libqt5-qtquickcontrols libqt5-qtquickcontrols2 libqt5-qtgraphicaleffects \
    # Other basic Qt based libraries
    libqca-qt5-devel \
    # For building documentation tarballs
    bzip2
RUN pip install gcovr
RUN gem install atspi cucumber
RUN ln -s /usr/bin/cucumber.ruby* /usr/bin/cucumber
# KDE stuff also depends on the following
RUN zypper --non-interactive in --allow-vendor-change \
    # modemmanager-qt
    ModemManager-devel \
    # networkmanager-qt
    NetworkManager-devel \
    # kauth
    polkit-devel \
    # kwindowsystem
    xcb-*-devel \
    # prison
    libdmtx-devel qrencode-devel \
    # kwayland
    wayland-devel \
    # baloo/kfilemetadata (some for okular)
    libattr-devel libexiv2-devel libtag-devel taglib-*-devel libepub-devel libpoppler-qt5-devel lmdb-devel \
    # kdoctools
    perl-URI docbook_4 docbook-xsl-stylesheets libxml2-devel libxslt-devel perl-URI \
    # khtml
    giflib-devel libopenssl-devel \
    # kdelibs4support
    libSM-devel \
    # kdnssd
    libavahi-devel libavahi-glib-devel libavahi-gobject-devel \
    # khelpcenter (and pim for grantlee)
    grantlee5-devel libxapian-devel \
    # sonnet
    hunspell-devel \
    # kio-extras and krdc
    libssh-devel \
    # plasma-pa
    gconf2-devel libpulse-devel libcanberra-devel \
    # user-manager
    libpwquality-devel \
    # sddm-kcm
    libXcursor-devel \
    # plasma-workspace
    libXtst-devel \
    # breeze-plymouth
    # plymouth-devel \
    # kde-gtk-config/breeze-gtk
    gtk3-devel gtk2-devel python3-cairo \
    # plasma-desktop/discover
    libAppStreamQt-devel fwupd-devel \
    # plasma-desktop
    xf86-input-synaptics-devel xf86-input-evdev-devel libxkbfile-devel xorg-x11-server-sdk \
    # kimpanel
    ibus-devel scim-devel \
    # libksane
    sane-backends-devel \
    # pim
    libical-devel libkolabxml-devel libxerces-c-devel \
    # <misc>
    alsa-devel libraw-devel fftw3-devel adobe-sourcecodepro-fonts \
    # choqok
    qoauth-qt5-devel qtkeychain-qt5-devel \
    # krita
    eigen3-devel OpenColorIO-devel dejavu-fonts gnu-free-fonts quazip-qt5-devel \
    # kaccounts / telepathy
    libaccounts-qt5-devel libaccounts-glib-devel libsignon-qt5-devel intltool \
    # skrooge
    sqlcipher sqlcipher-devel sqlite3-devel sqlite3 libofx-devel poppler-tools \
    # kwin
    libepoxy-devel Mesa-demo Mesa-demo-x xorg-x11-server-extra dmz-icon-theme-cursors libgbm-devel weston \
    xorg-x11-server-wayland \
    # kgraphviewer
    graphviz-devel \
    # drkonqi
    at-spi2-core which libgirepository-1_0-1 typelib-1_0-Atspi-2_0 gobject-introspection-devel \
    # kdevelop
    gdb \
    # labplot
    gsl-devel \
    # kuserfeedback
    php7 \
    # digikam
    QtAV-devel opencv-devel \
    # wacomtablet
    libwacom-devel \
    xf86-input-wacom-devel \
    # rust-qt-binding-generator
    rust rust-std \
    cargo \
    # kdevelop
    clang \
    clang-devel \
    llvm-devel \
    subversion-devel \
    python3-devel \
    # clazy
    clang-devel-static \
    # libkleo
    libqgpgme-devel \
    # akonadi
    mariadb libQt5Sql5-mysql \
    # libkdegames
    openal-soft-devel \
    libsndfile-devel \
    # kscd
    libmusicbrainz-devel \
    libmusicbrainz5-devel \
    # ktp-common-internals (also rest of KDE Telepathy)
    telepathy-qt5-devel \
    # audiocd-kio
    cdparanoia-devel \
    # ark
    libarchive-devel \
    # ffmpegthumbs
    libavcodec-devel libavfilter-devel libavformat-devel libavdevice-devel libavutil-devel libswscale-devel libpostproc-devel \
    # k3b
    flac-devel \
    libmad-devel \
    libmp3lame-devel \
    libogg-devel libvorbis-devel \
    libsamplerate-devel \
    # kamera
    libgphoto2-devel \
    # signon-kwallet-extension
    libsignon-glib-devel signond-libs-devel \
    # kdenlive
    libmlt-devel libmlt++-devel libmlt6-modules libmlt6-data rttr-devel \
    # print-manager
    cups-devel \
    # krfb
    LibVNCServer-devel \
    # kscd
    libdiscid-devel \
    # ktp-call-ui
    gstreamer-plugins-qt5-devel \
    # minuet
    fluidsynth-devel \
    # kajongg
    python3-Twisted \
    # okular
    texlive-latex \
    # ksmtp tests
    cyrus-sasl-plain \
    # kdb
    libmariadb-devel postgresql-devel \
    # Gwenview
    cfitsio-devel \
    # Calligra, Krita and probably other things elsewhere too
    libboost_*-devel \
    # Amarok
    gmock gtest libcurl-devel libofa-devel libgpod-devel libmtp-devel loudmouth-devel liblastfm-qt5-devel libmysqld-devel \
    # Cantor
    python2-numpy python3-numpy python3-matplotlib octave maxima \
    # KPat
    freecell-solver-devel \
    # RKWard
    R-base-devel gcc-fortran

# For D-Bus to be willing to start it needs a Machine ID
RUN dbus-uuidgen > /etc/machine-id

# OpenSUSE has a different GID for the 'video' group compared to the Physical Ubuntu and Debian hosts
# Therefore we create a group which matches the physical hosts 'video' group GID
# This is necessary to allow us to grant Jenkins access to the vgem device
RUN groupadd -g 44 docker-video
# We need a user account to do things as, and SSHD needs keys
RUN useradd -d /home/jenkins/ -u 1000 --user-group --create-home -G video,docker-video jenkins && /usr/sbin/sshd-gen-keys-start
# We want to run SSHD so that Jenkins can remotely connect to this container
EXPOSE 22
CMD ["/usr/sbin/sshd", "-D"]
